# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-05-24 14:09+0200\n"
"PO-Revision-Date: 2021-06-23 17:21+0000\n"
"Last-Translator: nihei <nihei@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Removing metadata using mat2\"]]\n"
msgstr "[[!meta title=\"Supprimer des métadonnées à l'aide de mat2\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, no-wrap
msgid "What is metadata?"
msgstr "Qu'est-ce que les métadonnées ?"

#. type: Plain text
msgid ""
"Metadata is \"data about data\" or \"information about information\" that is "
"embedded in computer files, usually automatically. Metadata is used to "
"describe, identify, categorize, and sort files."
msgstr ""
"Les métadonnées sont des « données sur les données » ou des « informations "
"sur les informations » qui sont intégrées dans les fichiers informatiques, "
"généralement de manière automatique. Les métadonnées sont utilisées pour "
"décrire, identifier, catégoriser et trier les fichiers."

#. type: Plain text
msgid ""
"However, metadata can also be used to deanonymize users and expose private "
"information."
msgstr ""
"Cependant, les métadonnées peuvent également être utilisées pour "
"désanonymiser les utilisateurs et exposer des informations privées."

#. type: Plain text
msgid "Examples of metadata include:"
msgstr "Voici quelques exemples de métadonnées :"

#. type: Bullet: '- '
msgid "In image files:"
msgstr ""

#. type: Bullet: '  - '
msgid "the location where a photo was taken"
msgstr ""

#. type: Bullet: '  - '
msgid "the date and time a photo was taken"
msgstr ""

#. type: Bullet: '  - '
msgid "the model and serial number of the camera used to take a photo"
msgstr ""

#. type: Bullet: '- '
msgid "In text document files:"
msgstr ""

#. type: Bullet: '  - '
msgid "the author of the document"
msgstr ""

#. type: Bullet: '  - '
msgid "changes to the document"
msgstr ""

#. type: Plain text
msgid ""
"To learn more about how metadata can be used to identify and reveal personal "
"information, see [Behind the Data: Investigating metadata](https://"
"exposingtheinvisible.org/en/guides/behind-the-data-metadata-investigations/)."
msgstr ""
"Pour en apprendre plus sur comment les métadonnées peuvent être utilisées "
"pour identifier et révéler des informations personnelles, voir [Behind the "
"Data: Investigating metadata](https://exposingtheinvisible.org/en/guides/"
"behind-the-data-metadata-investigations/) (en anglais)."

#. type: Plain text
#, no-wrap
msgid ""
"Removing metadata from files\n"
"==========================\n"
msgstr ""
"Supprimer les métadonnées des fichiers\n"
"======================================\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes\n"
"<a href=\"https://0xacab.org/jvoisin/mat2\"><span class=\"application\">mat2</span></a>\n"
"so you can remove metadata from files before you publish or share them.\n"
msgstr ""
"Tails inclus\n"
"<a href=\"https://0xacab.org/jvoisin/mat2\"><span class=\"application\">mat2</span></a>\n"
"pour que vous puissiez nettoyer les métadonnées de vos fichiers avant de les publier ou de les partager.\n"

#. type: Plain text
#, no-wrap
msgid "<span class=\"application\">mat2</span> works on many file formats, including:\n"
msgstr "<span class=\"application\">mat2</span> fonctionne sur de nombreux formats de fichiers,  notamment :\n"

#. type: Bullet: '- '
msgid "image files, such as .jpeg, .png, and .gif"
msgstr "les fichiers d'images, par exemple les .jpeg, .png, et les .gif"

#. type: Bullet: '- '
msgid ""
"<span class=\"application\">LibreOffice</span> files, such as .odt and .ods"
msgstr ""
"les fichiers <span class=\"application\">LibreOffice</span>, par exemple "
"les .odt et les .ods"

#. type: Bullet: '- '
msgid ""
"<span class=\"application\">Microsoft Office</span> documents, such as ."
"docx, .xlsx, and .pptx"
msgstr ""
"les documents <span class=\"application\">Microsoft Office</span>, par "
"exemple les .docx, .xlsx, et les .pptx"

#. type: Bullet: '- '
msgid "audio files, such as .mp3, .flac, and .ogg"
msgstr "les fichiers audio, par exemple les .mp3, .flac et les .ogg"

#. type: Bullet: '- '
msgid "video files, such as .mp4 and .avi"
msgstr "les fichiers vidéos, par exemple les .mp4 et les .avi"

#. type: Bullet: '- '
msgid "archive files, such as .zip and .tar"
msgstr "les fichiers d'archive, par exemple les .zip et les .tar"

#. type: Plain text
#, no-wrap
msgid ""
"To use <span class=\"application\">mat2</span> to remove metadata from your\n"
"files:\n"
msgstr ""
"Pour utiliser <span class=\"application\">mat2</span> afin de supprimer les métadonnées de vos\n"
"fichiers :\n"

#. type: Bullet: '1. '
msgid "Open the <span class=\"application\">Files</span> browser."
msgstr "Ouvrez le navigateur de <span class=\"application\">Fichiers</span>."

#. type: Bullet: '1. '
msgid ""
"Navigate to the folder containing the files that you want to remove metadata "
"from."
msgstr ""
"Naviguez jusqu'au dossier contenant les fichiers dont vous voulez supprimer "
"les métadonnées."

#. type: Bullet: '1. '
msgid "Select the files that you want to remove metadata from."
msgstr "Sélectionnez les fichiers dont vous voulez supprimer les métadonnées."

#. type: Bullet: '1. '
msgid ""
"Right-click (on Mac, click with two fingers) on the files and choose <span "
"class=\"guimenuitem\">Remove metadata</span>."
msgstr ""
"Faites un clic-droit (sur Mac, cliquez avec deux doigts) sur les fichiers et "
"choisissez <span class=\"guimenuitem\">Remove metadata</span> (« supprimer "
"les métadonnées »)."

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>It is impossible to reliably find and remove all metadata in complex file\n"
"formats. For example, <i>Microsoft Office</i> documents can contain embedded images,\n"
"audio, and other files containing their own metadata that <i>mat2</i> cannot\n"
"remove.</p>\n"
msgstr ""
"<p>Il est impossible de trouver et de supprimer de manière fiable toutes les métadonnées dans les formats de fichiers\n"
"complexes. Par exemple, les documents <i>Microsoft Office</i> peuvent contenir des images intégrées,\n"
"de l'audio et d'autres fichiers contenant leurs propres métadonnées que <i>mat2</i> ne peux pas\n"
"supprimer.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>You should run <i>mat2</i> on any files before you embed them into another\n"
"document.</p>\n"
msgstr ""
"<p>Vous devriez exécuter <i>mat2</i> sur tous les fichiers avant de les intégrer dans un autre\n"
"document.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Also, you should save files in simpler formats whenever possible.\n"
"For example, instead of saving a text document as a .docx file,\n"
"you can save the document as a plain .txt file.</p>\n"
msgstr ""
"<p>Vous devriez également enregistrer les fichiers dans des formats plus simple lorsque cela est possible.\n"
"Par exemple, au lieu d'enregistrer un document texte au format .docx,\n"
"vous pouvez l'enregistrer comme un simple fichier .txt.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#, no-wrap
#~ msgid ""
#~ "- In image files:\n"
#~ "  - the location where a photo was taken\n"
#~ "  - the date and time a photo was taken\n"
#~ "  - the model and serial number of the camera used to take a photo\n"
#~ "- In text document files:\n"
#~ "  - the author of the document\n"
#~ "  - changes to the document\n"
#~ msgstr ""
#~ "- Dans les fichiers d'images :\n"
#~ "  - Le lieu où la photo a été prise\n"
#~ "  - la date et l'heure où la photo a été prise\n"
#~ "  - le modèle et le numéro de série de l'appareil photo qui a été utilisé pour prendre la photo\n"
#~ "- Dans les documents textes :\n"
#~ "  - l'auteur du document\n"
#~ "  - les changements apportés au document\n"
