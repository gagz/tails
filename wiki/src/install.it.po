# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: italian\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-03-14 18:09+0000\n"
"PO-Revision-Date: 2021-03-02 23:44+0000\n"
"Last-Translator: gallium69 <gallium69@riseup.net>\n"
"Language-Team: ita <transitails@inventati.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Content of: <div>
#, fuzzy
#| msgid "[[!meta title=\"Download and install Tails\"]]"
msgid "[[!meta title=\"Get Tails\"]]"
msgstr "[[!meta title=\"Scarica ed installa Tails\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta title=\"Welcome to the Tails Installation Assistant\"]] [[!meta "
#| "stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
#| "stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] "
#| "[[!meta stylesheet=\"inc/stylesheets/router-install\" rel=\"stylesheet\" "
#| "title=\"\"]] [[!inline pages=\"install/inc/tails-installation-assistant."
#| "inline\" raw=\"yes\" sort=\"age\"]]"
msgid ""
"[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"install\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta title=\"Welcome to the Tails Installation Assistant\"]] [[!meta "
"stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"inc/stylesheets/router-install\" rel=\"stylesheet\" title="
"\"\"]] [[!inline pages=\"install/inc/tails-installation-assistant.inline.it"
"\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Windows"
msgstr "Windows"

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/win]]"
msgid "|install/windows]]"
msgstr "|install/win]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "macOS"
msgstr "macOS"

#. type: Content of: <div><div>
msgid "|install/mac]]"
msgstr "|install/mac]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div>
msgid "|install/linux]]"
msgstr "|install/linux]]"

#. type: Content of: <div><div>
msgid "[[ [[!img install/inc/icons/expert.png link=\"no\" alt=\"\"]]"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Terminal"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Debian or Ubuntu using the command line and GnuPG"
msgstr ""

#. type: Content of: <div><div>
#, fuzzy
#| msgid "|install/linux]]"
msgid "|install/expert]]"
msgstr "|install/linux]]"

#. type: Content of: <div><p>
msgid ""
"If you know someone you trust who uses Tails already, you can install your "
"Tails by cloning their Tails:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on PC|install/clone/pc]]"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "[[Install by cloning from another Tails on Mac|install/clone/mac]]"
msgstr ""

#. type: Content of: <h3>
msgid "Other options:"
msgstr ""

#. type: Content of: <ul><li>
#, fuzzy
#| msgid "[[For USB sticks (USB image)|install/download]]"
msgid "[[Download only (for USB sticks)|install/download]]"
msgstr "[[Per chiavette USB (immagine USB)|install/download]]"

#. type: Content of: <ul><li>
msgid "[[Burn a Tails DVD|install/dvd]]"
msgstr ""

#. type: Content of: <ul><li>
#, fuzzy
#| msgid "[[For virtual machines (ISO image)|install/vm-download]]"
msgid "[[Run Tails in a virtual machine|install/vm]]"
msgstr "[[Per macchine virtuali (immagine ISO)|install/vm-download]]"

#. type: Content of: <div><p>
msgid ""
"<strong>Tails doesn't work on smartphones or tablets.</strong> The hardware "
"of smartphones and tablets is very different from the hardware of "
"computers.  For now, it's impossible to make smartphone and tablet hardware "
"work with Linux distributions like Tails."
msgstr ""

#~ msgid "Thank you for your interest in Tails."
#~ msgstr "Grazie del tuo interesse per Tails."

#~ msgid ""
#~ "Installing Tails can be quite long but we hope you will still have a good "
#~ "time :)"
#~ msgstr ""
#~ "Installare Tails può richiedere parecchio tempo ma noi speriamo che ti "
#~ "diverta comunque :)"

#~ msgid ""
#~ "We will first ask you a few questions to choose your installation "
#~ "scenario and then guide you step by step."
#~ msgstr ""
#~ "Per prima cosa ti faremo alcune domande per scegliere il tuo scenario "
#~ "d'installazione e poi ti guideremo passo per passo."

#~ msgid "Which operating system are you installing Tails from?"
#~ msgstr "Da quale sistema operativo stai installando Tails?"

#~ msgid "[["
#~ msgstr "[["

#~ msgid "Download only:"
#~ msgstr "Scaricare soltanto:"

#~ msgid "[[For DVDs (ISO image)|install/dvd-download]]"
#~ msgstr "[[Per DVD (immagine ISO)|install/dvd-download]]"

#~ msgid "Debian, Ubuntu, or Mint"
#~ msgstr "Debian, Ubuntu o Mint"

#~ msgid "|install/debian]]"
#~ msgstr "|install/debian]]"

#~ msgid "<small>(Red Hat, Fedora, etc.)</small>"
#~ msgstr "<small>(Red Hat, Fedora, etc.)</small>"

#~ msgid "Let's start the journey!"
#~ msgstr "Iniziamo!"

#~ msgid "Welcome to the"
#~ msgstr "Benvenuta nella"

#~ msgid "<strong>Tails Installation Assistant</strong>"
#~ msgstr "<strong>Installazione assistita di Tails</strong>"

#~ msgid ""
#~ "The following set of instructions is quite new. If you face problems "
#~ "following them:"
#~ msgstr ""
#~ "Il seguente set di istruzioni è relativamente nuovo, se incontri un "
#~ "problema seguendolo, per piacere:"

#~ msgid "[[Report your problem.|support/talk]]"
#~ msgstr "[[Riferisci il problema.|support/talk]]"

#~ msgid ""
#~ "Try following our old instructions for [[downloading|/download]] or "
#~ "[[installing|doc/first_steps]] instead."
#~ msgstr ""
#~ "Prova altrimenti a seguire le vecchie istruzioni per [[scaricare|/"
#~ "download]] o [[installare|doc/first_steps]]."
